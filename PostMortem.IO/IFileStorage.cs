﻿using PostMortem.IO.Models;
using System;
using System.IO;
using System.Threading.Tasks;

namespace PostMortem.IO
{
    public interface IFileStorage
    {
        Task<bool> StoreTextFile(string text, string filePath);

        Task<string> ReadTextFile(string filePath);

    }
}
