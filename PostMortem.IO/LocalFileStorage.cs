﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using PostMortem.IO.Models;

namespace PostMortem.IO
{
    public class LocalFileStorage : IFileStorage
    {
        public async Task<string> ReadTextFile(string filePath)
        {
            try
            {
                return await File.ReadAllTextAsync(filePath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "";
            }
        }

        public async Task<bool> StoreTextFile(string text, string filePath)
        {
            try
            {
                await File.WriteAllTextAsync(filePath, text);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}
