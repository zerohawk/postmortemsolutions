﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostMortem.IO.Models
{
    public class WebFile
    {
        public string FilePath { get; set; }

        public byte[] Data { get; set; }
    }
}
