﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostMortem.Utils.Extensions
{
    public static class HtmlNodeExtensions
    {
        private static StringComparison _defaultCompare = StringComparison.OrdinalIgnoreCase;

        public static IEnumerable<HtmlNode> WhereByClassName(this IEnumerable<HtmlNode> htmlNodes, string className)
        {
            return htmlNodes.Where(node => node.HasClass(className));
        }

        public static HtmlNode SingleByClassName(this IEnumerable<HtmlNode> htmlNodes, string className)
        {
            return htmlNodes.Single(node => node.HasClass(className));
        }

        public static IEnumerable<HtmlNode> WhereByTag(this IEnumerable<HtmlNode> htmlNodes, string tagName)
        {
            return htmlNodes.Where(node => node.Name.Equals(tagName, _defaultCompare));
        }

        public static HtmlNode GetSingleNodeByTag(this IEnumerable<HtmlNode> htmlNodes, string tagName)
        {
            return htmlNodes.SingleOrDefault(node => node.Name.Equals(tagName, _defaultCompare));
        }

        public static HtmlNode GetChildNodeByTag(this HtmlNode htmlNode, string tagName)
        {
            return GetSingleNodeByTag(htmlNode.ChildNodes, tagName);
        }

        public static string GetAttributeValue(this HtmlNode htmlNode, string attributeName)
        {
            return htmlNode.Attributes.Single(atrib => atrib.Name.Equals(attributeName, _defaultCompare)).Value;
        }
    }
}
