﻿using PostMortemBot.PopFlash.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using PostMortem.Utils.Extensions;
using System.Linq;
using PostMortemBot.Storage;
using Microsoft.Extensions.Logging;

namespace PostMortemBot.PopFlash
{
    public class PopFlashCrawler
    {
        private LiteDBStorage _dBStorage;
        private readonly ILogger<PopFlashCrawler> _logger;
        private HttpClient _httpClient;

        public PopFlashCrawler(LiteDBStorage dBStorage, ILogger<PopFlashCrawler> logger)
        {
            _dBStorage = dBStorage;
            _logger = logger;
        }


        public async Task CrawlUsersRecentMatches(string userId)
        {
            _httpClient = new HttpClient()
            {
                BaseAddress = new Uri("https://popflash.site/")
            };

            var response = await _httpClient.GetAsync($"user/{userId}");

            var htmlString = await response.Content.ReadAsStringAsync();

            var document = new HtmlDocument();
            document.LoadHtml(htmlString);

            var rows = document.DocumentNode.Descendants("div")
                .SingleByClassName("latest-matches")
                .Descendants()
                .WhereByTag("tr")
                .Skip(1); //Don't need the header

            var player = _dBStorage.GetPlayer(userId);

            foreach (var row in rows)
            {
                var matchUrl = row.Descendants("td")
                    .First()
                    .GetChildNodeByTag("a")
                    .GetAttributeValue("href");
                var id = matchUrl.Split("match/").Last();

                var match = new Match() { WebId = id, Finished = false };

                var dbMatch = _dBStorage.GetMatch(id);

                if (dbMatch == null)
                {
                    _dBStorage.Create(match);

                    dbMatch = _dBStorage.GetMatch(id);

                    player.RecentMatches.Add(dbMatch);

                    _dBStorage.Update(player);
                }
            }

        }

        public async Task CrawlStoredMatch(Match match)
        {
            _httpClient = new HttpClient()
            {
                BaseAddress = new Uri("https://popflash.site/")
            };

            var response = await _httpClient.GetAsync($"match/{match.WebId}");

            var htmlString = await response.Content.ReadAsStringAsync();

            var document = new HtmlDocument();
            document.LoadHtml(htmlString);

            match.Date = document.DocumentNode.Descendants("div")
                .First(node => node.Id == "match-container")
                .GetChildNodeByTag("h2")
                .GetChildNodeByTag("span")
                .InnerText;

            var scoreText = document.DocumentNode.Descendants("div")
                .SingleByClassName("score-1")
                .ParentNode
                .InnerText
                .Trim()
                .Split("\n")
                .Select(s => s.Trim())
                .Where(s => !string.IsNullOrEmpty(s))
                .ToArray();

            match.Score = $"{scoreText[0]}-{scoreText[1]}";
            match.Type = scoreText[2];
            match.Map = scoreText[3];
            match.Finished = scoreText[5].Contains("Match is final");

            var tables = document.DocumentNode.Descendants("div")
                .SingleByClassName("scoreboards")
                .Descendants("div")
                .Select(div => div.GetChildNodeByTag("table"))
                .Where(d => d != null);

            var team1 = tables.First();
            var team2 = tables.Last();

            var rows = team1.Descendants("tr")
                    .Skip(1);

            foreach (var row in rows)
            {
                var playerScore = _readTableRow(row);

                if (playerScore != null)
                {
                    match.Team1.Add(playerScore);
                }
            }

            rows = team2.Descendants("tr")
                    .Skip(1);

            foreach (var row in rows)
            {
                var playerScore = _readTableRow(row);

                if (playerScore != null)
                {
                    match.Team2.Add(playerScore);
                }
            }

            _dBStorage.Update(match);
        }


        private PlayerScore _readTableRow(HtmlNode htmlNode)
        {
            var tds = htmlNode.Descendants("td").ToArray();

            if(tds[1].InnerText == "0" 
                && tds[2].InnerText == "0" 
                && tds[3].InnerText == "0" 
                && tds[5].InnerText == "0")
            {
                return null;
            }

            return new PlayerScore()
            {
                PlayerId = tds[0].GetChildNodeByTag("a")
                    .GetAttributeValue("href")
                    .Split("user/").Last(),
                PlayerName = tds[0].GetAttributeValue("title"),
                Kills = tds[1].InnerText,
                Assists = tds[2].InnerText,
                Deaths = tds[3].InnerText,
                FlashAssists = tds[4].InnerText,
                AverageDamagePerRound = tds[5].InnerText,
                HLTVRating = tds[6].InnerText,
                HeadShotPercentage = tds[7].InnerText,
                ClutchKills = tds[8].InnerText,
                BombsPlanted = tds[9].InnerText,
                BombsDefused = tds[10].InnerText,
                FlashedEnemyDuration = tds[11].InnerText
            };
        }
    }
}
