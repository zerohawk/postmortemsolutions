﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PostMortem.Utils;
using PostMortemBot.Storage;

namespace PostMortemBot.PopFlash
{
    public class PopFlashService : BackgroundService
    {
        private readonly ILogger<PopFlashService> _logger;
        private readonly LiteDBStorage _dBStorage;
        private readonly PopFlashCrawler _popFlashCrawler;

        private bool _isLooking;
        private DateTime TimeSinceStartingToQuery;

        public PopFlashService(ILogger<PopFlashService> logger, LiteDBStorage dBStorage, PopFlashCrawler popFlashCrawler)
        {
            _logger = logger;
            _dBStorage = dBStorage;
            _popFlashCrawler = popFlashCrawler;
        }

        public void LookForActiveMatches()
        {
            _isLooking = true;
            TimeSinceStartingToQuery = DateTime.Now;
        }

        protected async override Task Process()
        {
            try
            {
                if (_isLooking)
                {
                    await _checkPlayerProfiles();
                }
                await _processActiveMatches();

                if(_isLooking && TimeSinceStartingToQuery.AddMinutes(10) < DateTime.Now)
                {
                    _isLooking = false;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"{e.Message}, \n {e.StackTrace}");
            }
            

        }

        private async Task _checkPlayerProfiles()
        {
            var players = _dBStorage.GetAllPlayers();

            foreach (var player in players)
            {
                _logger.LogInformation($"Checking for new matches on player {player.WebId}");
                await _popFlashCrawler.CrawlUsersRecentMatches(player.WebId);
            }
        }

        private async Task _processActiveMatches()
        {
            var matches = _dBStorage.GetAllMatches();

            foreach (var match in matches)
            {
                if (!match.Finished)
                {
                    _isLooking = false; //we found a unfinished map

                    await _popFlashCrawler.CrawlStoredMatch(match);
                    _logger.LogInformation($"Reading match data: {match.WebId}");
                }
            }
        }
    }
}
