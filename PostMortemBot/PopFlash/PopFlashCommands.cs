﻿using Discord;
using Microsoft.Extensions.Hosting;
using PostMortemBot.PopFlash.Models;
using PostMortemBot.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PostMortemBot.PopFlash
{
    public class PopFlashCommands
    {
        private readonly LiteDBStorage _dbStorage;
        private readonly PopFlashCrawler _popFlashCrawler;
        private readonly PopFlashService _popFlashService;

        private Dictionary<string, Expression<Func<PlayerScore, object>>> _optionToFilter;

        public PopFlashCommands(LiteDBStorage dbStorage, PopFlashCrawler popFlashCrawler, IHostedService popFlashService)
        {
            _dbStorage = dbStorage;
            _popFlashCrawler = popFlashCrawler;
            _popFlashService = popFlashService as PopFlashService;
            _optionToFilter = new Dictionary<string, Expression<Func<PlayerScore, object>>>()
            {
                { "Kills", (playerScore) => int.Parse(playerScore.Kills) },
                { "Assists", (playerScore) => int.Parse(playerScore.Assists) },
                { "Deaths", (playerScore) => int.Parse(playerScore.Deaths) },
                { "KD", (playerScore) => float.Parse(playerScore.Kills) / float.Parse(playerScore.Deaths) },
                { "ADR", (playerScore) => int.Parse(playerScore.AverageDamagePerRound) },
                { "BD", (playerScore) => int.Parse(playerScore.BombsDefused) },
                { "BP", (playerScore) => int.Parse(playerScore.BombsPlanted) },
                { "CK", (playerScore) => int.Parse(playerScore.ClutchKills) },
                { "HS", (playerScore) => float.Parse(playerScore.HeadShotPercentage) },
                { "FA", (playerScore) => int.Parse(playerScore.FlashAssists) },
                { "FED", (playerScore) => float.Parse(playerScore.FlashedEnemyDuration) },
                { "HLTV", (playerScore) => float.Parse(playerScore.HLTVRating) }
            };
        }

        public async Task<CommandResult> TrackUser(string[] parameters)
        {
            var userId = parameters[0];

            if (!userId.Trim().All(c => Char.IsDigit(c)))
            {
                return new CommandResult { Message = $"UserId {userId} incorrectly formatted.", Success = false };
            }

            if(_dbStorage.Create(new Player { WebId = userId }))
            {
                return new CommandResult { Message = $"Player {userId} stored successfully", Success = true };
            }

            return new CommandResult { Message = $"Cannot {nameof(TrackUser)} Player.", Success = false };
        }

        public async Task<CommandResult> UnTrackUser(string[] parameters)
        {
            var userId = parameters[0];

            if (!userId.Trim().All(c => Char.IsDigit(c)))
            {
                return new CommandResult { Message = $"UserId {userId} incorrectly formatted.", Success = false };
            }

            if (_dbStorage.Remove(new Player { WebId = userId }))
            {
                return new CommandResult { Message = $"Removed Player {userId} from tracklist", Success = true };
            }

            return new CommandResult { Message = $"Cannot {nameof(UnTrackUser)} Player.", Success = false };
        }

        public async Task<CommandResult> RemoveMatch(string[] parameters)
        {
            var dbMatch = _dbStorage.GetMatch(parameters[0]);

            if (dbMatch == null)
            {
                return new CommandResult { Message = $"Cannot find this match", Success = true };
            }

            _dbStorage.Remove(dbMatch);

            return new CommandResult { Message = "Removed Match", Success = true };
        }

        public async Task<CommandResult> GetAllMatches(string[] parameters)
        {
            var allMatches = _dbStorage.GetAllMatches().ToList();

            var message = $"Currently {allMatches.Count} match stats stored \n";
            foreach(var match in allMatches)
            {
                message += $"MatchId: {match.WebId}, Score: {match.Score}";
            }

            return new CommandResult { Message = message, Success = true };
        }

        public async Task<CommandResult> StartQueryForNewMatch(string[] parameters)
        {
            var dbMatch = _dbStorage.GetMatch(parameters[4]);

            if(dbMatch != null)
            {
                return new CommandResult { Message = $"We've already analysed this match", Success = true };
            }
            var match = new Match { WebId = parameters[4], Finished = false };

            _dbStorage.Create(match);

            await _popFlashCrawler.CrawlStoredMatch(match);

            return new CommandResult { Message = $"Reading Match data", Success = true };
        }

        public async Task<CommandResult> LeaderBoards(string[] parameters)
        {
            if (!_optionToFilter.Keys.Contains(parameters[0]))
            {
                return new CommandResult { Success = true, Message = $"Incorrect argument" };
            }
            var isFloat = new string[] { "HS", "FED", "HLTV", "KD" }.Contains(parameters[0]);

            var matches = _dbStorage.GetAllMatches();

            var allPlayerScores = matches.SelectMany(m => m.Team1.Concat(m.Team2));

            var scoreByPlayers = allPlayerScores.GroupBy(ps => ps.PlayerId)
                .Where(ps => ps.Count() > 3);

            var playerByAverageFloat = new Dictionary<string, float>();
            var playerByAverageInt = new Dictionary<string, int>();

            foreach (var player in scoreByPlayers)
            {
                var overallAverage = player.Select(_optionToFilter[parameters[0]].Compile());
                var name = player.First().PlayerName;
                if (isFloat)
                {
                    playerByAverageFloat.Add(name, overallAverage.Cast<float>().Average());
                }
                else
                {
                    playerByAverageInt.Add(name, (int)overallAverage.Cast<int>().Average());
                }
            }

            var comResult = new CommandResult { Embed = new Discord.EmbedBuilder() };

            comResult.Embed.Color = Color.DarkOrange;

            comResult.Embed.Author = new EmbedAuthorBuilder().WithName("For issues contact Duke");

            if (isFloat)
            {
                var scores = playerByAverageFloat.OrderByDescending(i => i.Value).Take(10).ToList();

                for (int i = 0; i < scores.Count(); i++)
                {
                    comResult.Embed.AddField($"{(i + 1)}# {scores[i].Key} - {scores[i].Value}", "====", false);
                }
            }
            else
            {
                var scores = playerByAverageInt.OrderByDescending(i => i.Value).Take(10).ToList();

                for (int i = 0; i < scores.Count(); i++)
                {
                    comResult.Embed.AddField($"{(i + 1)}# {scores[i].Key} - {scores[i].Value}", "====", false);
                }

            }
            comResult.Success = true;

            return comResult;
        }
        

        public async Task<CommandResult> RecentMatchHighLights(string[] parameters)
        {
            var matches = _dbStorage.GetAllMatches();

            var recentMatch = matches.OrderByDescending(i => DateTime.Parse(i.Date)).FirstOrDefault();

            if(recentMatch == null)
            {
                return new CommandResult { Success = false, Message = $"Cannot find matches" };
            }
            var players = recentMatch.Team1.ToList();
            players.AddRange(recentMatch.Team2);

            var topFragger = players.OrderByDescending(p => int.Parse(p.AverageDamagePerRound)).First();
            var topHltv = players.OrderByDescending(p => float.Parse(p.HLTVRating)).First();
            var topHsPercent = players.OrderByDescending(p => float.Parse(p.HeadShotPercentage)).First();
            var topUtil = players.OrderByDescending(p => float.Parse(p.FlashedEnemyDuration)).First();

            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine($"Results: {Environment.NewLine}");
            stringBuilder.AppendLine($"Name | Kills | Assists | Deaths");
            stringBuilder.AppendLine("================");
            stringBuilder.AppendLine($"{Environment.NewLine}");
            stringBuilder.AppendLine($"Team1:");
            foreach (var player in recentMatch.Team1)
            {
                stringBuilder.AppendLine($"{player.PlayerName} | {player.Kills} | {player.Assists} | {player.Deaths} ");
            }
            stringBuilder.AppendLine($"{Environment.NewLine}");
            stringBuilder.AppendLine($"Team2:");
            foreach (var player in recentMatch.Team2)
            {
                stringBuilder.AppendLine($"{player.PlayerName} | {player.Kills} | {player.Assists} | {player.Deaths} ");
            }
            stringBuilder.AppendLine($"{Environment.NewLine}");

            stringBuilder.AppendLine($"Highest damage per round: {topFragger.PlayerName} with {topFragger.AverageDamagePerRound}.");
            stringBuilder.AppendLine($"HLTV Star: {topHltv.PlayerName} with {topHltv.HLTVRating} HLTVRating.");
            stringBuilder.AppendLine($"Next Scream: {topHsPercent.PlayerName} with a Headshot percentage of {topHsPercent.HeadShotPercentage}.");
            stringBuilder.AppendLine($"Utility God: {topUtil.PlayerName} with a EnemyFlash Duration of {topUtil.FlashedEnemyDuration}.");

            stringBuilder.AppendLine($"{Environment.NewLine}");
            stringBuilder.AppendLine("GG bois");

            return new CommandResult { Message = stringBuilder.ToString(), Success = true };
        }
    }
}
