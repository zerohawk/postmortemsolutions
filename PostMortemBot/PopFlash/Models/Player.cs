﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Text;

namespace PostMortemBot.PopFlash.Models
{
    public class Player
    {
        public ObjectId _id { get; set; }

        public string WebId { get; set; }

        public List<Match> RecentMatches { get; set; } = new List<Match>();
    }
}
