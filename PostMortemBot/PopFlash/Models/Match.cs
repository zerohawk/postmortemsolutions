﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Text;

namespace PostMortemBot.PopFlash.Models
{
    public class Match
    {
        public ObjectId _id { get; set; }

        public string WebId { get; set; }

        public string Date { get; set; }

        public string Score { get; set; }

        public string Map { get; set; }

        public List<PlayerScore> Team1 { get; set; } = new List<PlayerScore>();

        public List<PlayerScore> Team2 { get; set; } = new List<PlayerScore>();

        public string Type { get; set; }

        public bool Finished { get; set; }
    }

    public class PlayerScore
    {
        public string PlayerName { get; set; }

        public string PlayerId { get; set; }

        public string Kills { get; set; }

        public string Assists { get; set; }

        public string Deaths { get; set; }

        public string FlashAssists { get; set; }

        public string AverageDamagePerRound { get; set; }

        public string HLTVRating { get; set; }

        public string HeadShotPercentage { get; set; }

        public string ClutchKills { get; set; }

        public string BombsPlanted { get; set; }

        public string BombsDefused { get; set; }

        public string FlashedEnemyDuration { get; set; }
    }

    public enum Result
    {
        Win,
        Lose,
        Tie
    }
}
