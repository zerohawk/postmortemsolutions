﻿using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PostMortemBot.PopFlash;
using PostMortemBot.Storage;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PostMortemBot
{
    class MainBot
    {
        private DiscordSocketClient _client;
        private IConfiguration _config;


        private ILogger<MainBot> _logger;
        private ServiceProvider _serviceProvider;


        public static void Main(string[] args)
        => new MainBot().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            _serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddScoped<PopFlashCommands>()
                .AddScoped<PopFlashCrawler>()
                .AddScoped<CommandHandler>()
                .AddScoped<LiteDBStorage>()
                .AddSingleton<IHostedService, PopFlashService>()
                .BuildServiceProvider();

            _logger = _serviceProvider.GetService<ILoggerFactory>()
                .AddConsole()
                .CreateLogger<MainBot>();

            //var crawler = _serviceProvider.GetService<PopFlashCrawler>();

            //await crawler.CrawlUsersRecentMatches("20101");

            _populateCommands();

            await _startDiscordBot();
        }

        private void _populateCommands()
        {
            var commandHandler = _serviceProvider.GetService<CommandHandler>();
            var popFlashCommands = _serviceProvider.GetService<PopFlashCommands>();

            commandHandler
                .AddCommand("https://popflash.site/match/", (input) => popFlashCommands.StartQueryForNewMatch(input))
                .AddCommand("!recent", (input) => popFlashCommands.RecentMatchHighLights(input))
                .AddCommand("!rem", (input) => popFlashCommands.RemoveMatch(input))
                .AddCommand("!leaderboards", (input) => popFlashCommands.LeaderBoards(input));
        }

        private async Task _startDiscordBot()
        {
            _client = new DiscordSocketClient();

            _client.Log += Log;

            await _client.LoginAsync(TokenType.Bot, "NTkwMTE5NzE3Nzk3MTAxNTY4.XRDR_w.JSzk7H-To1lBTktxujTJTAjaiog");
            await _client.StartAsync();

            //var popFlashService = _serviceProvider.GetService<IHostedService>() as PopFlashService;
            //await popFlashService.StartAsync(new System.Threading.CancellationToken());

            _client.MessageReceived += MessageReceived;

            await Task.Delay(-1);
        }

        private Task Log(LogMessage msg)
        {
            _logger.LogInformation(msg.ToString());
            return Task.CompletedTask;
        }

        private async Task MessageReceived(SocketMessage message)
        {
            if(message.Author.Username == _client.CurrentUser.Username) //Don't process own messages?
            {
                return;
            }
            _logger.LogInformation($"{message.Author.Username}: {message.Content}");

            var commandHandler = _serviceProvider.GetService<CommandHandler>();

            var commandResult = await commandHandler.ExecuteCommand(message.Content);

            if (commandResult.Success)
            {
                if(commandResult.Embed != null)
                {
                    await message.Channel.SendMessageAsync("", false, commandResult.Embed.Build());
                }
                else
                {
                    await message.Channel.SendMessageAsync(commandResult.Message);
                }
            }
            else
            {
                await Log(new LogMessage(LogSeverity.Info, "", commandResult.Message));
            }
        }
    }
}
