﻿using LiteDB;
using PostMortemBot.PopFlash.Models;
using System;
using System.Collections.Generic;

namespace PostMortemBot.Storage
{
    public class LiteDBStorage
    {
        public bool Create(Player player)
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var col = db.GetCollection<Player>("players");

                col.Insert(player);

                return col.Exists(p => p.WebId == player.WebId);
            }
        }

        public Player GetPlayer(string id)
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var col = db.GetCollection<Player>("players");

                return col.GetPlayerByCustomId(id);
            }
        }

        public IEnumerable<Player> GetAllPlayers()
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var col = db.GetCollection<Player>("players");

                return col.FindAll();
            }
        }

        public IEnumerable<Match> GetAllMatches()
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var col = db.GetCollection<Match>("matches");

                return col.FindAll();
            }
        }

        public bool Update(Player player)
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var col = db.GetCollection<Player>("players");

                return col.Update(player);
            }
        }

        public bool Remove(Player player)
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var col = db.GetCollection<Player>("players");

                var model = col.GetPlayerByCustomId(player.WebId);

                if (col.Delete(model._id))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void Create(Match match)
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var col = db.GetCollection<Match>("matches");

                col.Insert(match);
            }
        }

        public Match GetMatch(string id)
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var col = db.GetCollection<Match>("matches");

                return col.GetMatchByCustomId(id);
            }
        }

        public bool Update(Match match)
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var col = db.GetCollection<Match>("matches");

                return col.Update(match);
            }
        }

        public bool Remove(Match player)
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var col = db.GetCollection<Match>("matches");

                if (col.Delete(p => p.WebId.ToLower() == player.WebId.ToLower()) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
