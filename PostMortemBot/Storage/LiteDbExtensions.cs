﻿using LiteDB;
using PostMortemBot.PopFlash.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PostMortemBot.Storage
{
    public static class LiteDbExtensions
    {
        public static Player GetPlayerByCustomId(this LiteCollection<Player> source, string Id)
        {
            return source.FindAll().FirstOrDefault(p => p.WebId.ToLower() == Id.ToLower());
        }

        public static Match GetMatchByCustomId(this LiteCollection<Match> source, string Id)
        {
            return source.FindAll().FirstOrDefault(p => p.WebId.ToLower() == Id.ToLower());
        }
    }
}
