﻿using PostMortemBot.PopFlash;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PostMortemBot
{
    public class CommandHandler
    {
        private Dictionary<string, Func<string[], Task<CommandResult>>> _commandToFunc = new Dictionary<string, Func<string[], Task<CommandResult>>>();

        public CommandHandler AddCommand(string command, Expression<Func<string[], Task<CommandResult>>> expression)
        {
            _commandToFunc.Add(command, expression.Compile());
            return this;
        }

        public async Task<CommandResult> ExecuteCommand(string command)
        {
            string detectedCommand = "";
            foreach(var possibleCommand in _commandToFunc.Keys)
            {
                if (command.ToLower().StartsWith(possibleCommand.ToLower()))
                {
                    detectedCommand = possibleCommand;
                }
            }

            if(_commandToFunc.TryGetValue(detectedCommand, out Func<string[], Task<CommandResult>> commandFunc))
            {
                string[] parameters;
                if (!command.Contains("http"))
                {
                    parameters = command.Split(" ").Skip(1).ToArray();
                }
                else
                {
                    parameters = command.Split("/");
                }

                return await commandFunc(parameters);
            }
            return new CommandResult { Success = false };
        }
    }
}
